<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

if (! isset($versionList) || ! is_array($versionList)) {
	echo "This file should be used as template, as part of the application, should not be called directly";
	return;
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tiki Packages - for manual installation</title>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800,900" rel="stylesheet">
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="css/versions.css">
	<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
</head>
<body>

<div class="header">
	<h1>Tiki Composer<br>Packages</h1>
	<div class="btns">
		<?php
		$counter = 1;
		foreach ($versionList as $version => $versionConfig) {
			?>
			<div class="btn btn0<?php echo $counter; ?>"><a href="#version0<?php echo $counter; ?>"><h4>
						Tiki <?php echo strtoupper($version); ?></h4></a></div>
			<?php
			$counter++;
		}
		?>
	</div>
</div>

<section>
	<h2>Overview</h2>
	<p class="p-big">Also called the Composer Web Installer, and introduced in Tiki18, this feature is for the installation and
		management of external software packages using Composer</p>
	<p>Composer is used in the Tiki code development process to import and manage external software such as jQuery and
		Bootstrap. But some software cannot be packaged with Tiki due to an incompatible license, or shouldn’t
		necessarily be packaged with Tiki by default because of the software’s specialized nature or niche application.
		So it is a natural step for Tiki site administrators to be able to use Composer to install and manage external
		software specifically for their site after the site is installed. </p>
	<p>You can read all the details directly from the Tiki documentation website at
		<strong><a href="https://doc.tiki.org/Packages">https://doc.tiki.org/Packages</a></strong>
	</p>
	<p>For software developers, please head over to 
		<strong><a href="https://dev.tiki.org/packages.tiki.org">https://dev.tiki.org/packages.tiki.org</a></strong> or
        <strong><a href="https://dev.tiki.org/composer-tiki-org">https://dev.tiki.org/composer-tiki-org</a></strong>
	</p>	
</section>


<section>
	<h2>How to use</h2>
	<p>Download from the list of packages below the one corresponding to your version of Tiki, currently there are
		packages provided for
		<?php
		$counter = 1;
		foreach ($versionList as $version => $versionConfig) {
			?>
			<a href="#version0<?php echo $counter; ?>" class="versions v0<?php echo $counter; ?>">Tiki <?php echo strtoupper($version); ?></a>
			<?php
			$counter++;
			if ($counter < count($versionList)) {
				echo ', ';
			}
			if ($counter == count($versionList)) {
				echo ' and ';
			}
		} ?> .</p>
	<p>After downloading the zip file, extract the zip file and copy the resulting folder to your Tiki installation,
		inside the folder "vendor_custom" that is located in the root folder of your Tiki installation, like the example
		below for the package "PdfJs"</p>
	<img src="img/illustration.svg" class="image has-shadow"></img>
	<p>And it's done! Tiki will pick up the package automatically and you will be able to use it as soon as you enable
		the preference that requires that specific package.</p>
</section>

<?php
$counter = 1;
foreach ($versionList as $version => $versionConfig) {
	?>
	<section id="version0<?php echo $counter; ?>" class="version0<?php echo $counter; ?>">
		<h2 class="title-versions">Tiki <?php echo strtoupper($version); ?></h2>
		<div class="container">
			<?php
			foreach ($versionConfig['packages'] as $packageName => $packageConfig) {
				?>
				<div class="item has-shadow">
					<a href="<?php echo $packagesFolder . '/' . $packageConfig['zipFile']; ?>" class="link">
						<div class="info">
							<h3><?php echo $packageName; ?></h3>
							<p class="p-small"><span class="semibold">Licence:</span> <?php echo $packageConfig['licence']; ?></p>
							<p class="p-small"><span class="semibold">Name:</span> <?php echo $packageConfig['name']; ?></p>
						</div>
						<div class="download">
							<svg viewBox="0 0 234 164.8" height="30">
								<path id="download-ico" class="ico" d="M205.6,114.7v34.2c0,8.8-7.2,16-16,16H44.3c-8.8,0-16-7.2-16-16v-34.2c0-4.1,3.4-7.5,7.5-7.5
                            s7.5,3.4,7.5,7.5v34.2c0,0.6,0.4,1,1,1h145.3c0.6,0,1-0.4,1-1v-34.2c0-4.1,3.4-7.5,7.5-7.5S205.6,110.6,205.6,114.7z M63.2,83.5
                            l48.9,41.5c0,0,0,0,0,0c0.3,0.3,0.6,0.5,1,0.7c0,0,0.1,0.1,0.1,0.1c0.3,0.2,0.7,0.4,1.1,0.5c0.1,0,0.1,0,0.2,0.1
                            c0.4,0.1,0.7,0.2,1.1,0.3c0.1,0,0.1,0,0.2,0c0.4,0.1,0.8,0.1,1.2,0.1c0.4,0,0.8,0,1.2-0.1c0.1,0,0.1,0,0.2,0
                            c0.4-0.1,0.8-0.2,1.1-0.3c0.1,0,0.1,0,0.2-0.1c0.4-0.1,0.7-0.3,1.1-0.5c0,0,0.1-0.1,0.1-0.1c0.4-0.2,0.7-0.4,1-0.7c0,0,0,0,0,0
                            l48.9-41.5c3.2-2.7,3.5-7.4,0.9-10.6c-2.7-3.2-7.4-3.5-10.6-0.9l-36.5,31V7.5c0-4.1-3.4-7.5-7.5-7.5c-4.1,0-7.5,3.4-7.5,7.5v95.6
                            L73,72.1c-3.2-2.7-7.9-2.3-10.6,0.9C59.7,76.1,60.1,80.8,63.2,83.5z"/>
							</svg>
							<p class="p-small">download .zip</p>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
	</section>
	<?php
	$counter++;
}
?>
</body>

<script type="text/javascript">
	$('a').click(function () {
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top
		}, 750);
		return false;
	});
</script>
</html>
